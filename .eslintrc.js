module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: ['./config/eslintconfig.js', 'plugin:react/recommended', 'standard', 'plugin:storybook/recommended'],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  plugins: ['react', '@typescript-eslint'],
  rules: {}
}
