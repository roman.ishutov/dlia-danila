import path from 'path'
import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

const mode = process.env.NODE_ENV
const isProd = mode === 'production'
const config = {
  mode,
  build: {
    outDir: path.resolve(__dirname, 'dist'),
    sourcemap: isProd,
    emptyOutDir: isProd,
    App: {
      entry: path.resolve(__dirname, 'src/main.tsx'),
      name: '[name]',
      fileName: ext => `[name]/[name].${ext}.ts`,
      formats: ['es', 'cjs']
    },
    plugins: [
      react({
        babel: {
          plugins: [
            ['@babel/plugin-proposal-decorators', { legacy: true }],
            ['@babel/plugin-proposal-class-properties']
          ]
        }
      })
    ]
  }
}

export default defineConfig(config)
